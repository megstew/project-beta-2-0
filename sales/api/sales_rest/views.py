from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import (
    AutomobileVO,
    SalesPerson,
    Customer,
    Sale,
)


class SalespersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "sales_person",
        "customer",
        "automobile",
        "id",
    ]
    encoders = {
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalespersonEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    ''' GET all instances of Salespeople or
    CREATE an instance of Salesperson '''
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        raise Exception("Unsupported Method!", request)


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, pk):
    ''' GET or DELETE an instance of Salesperson '''
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson ID"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        raise Exception("Unsupported Method!", request)


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    '''GET ALL instances of Customer and
    CREATE an instance of Customer'''
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        raise Exception("Unsupports Method!", request)


@require_http_methods(["GET", "DELETE"])
def api_customer(request, pk):
    '''GET an instance of Customer and
    DELETE an instance of Customer'''
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        raise Exception("Unsupported Method!", request)


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    '''GET list of sale instances and CREATE a sale instance'''
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales_person"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer"},
                status=400,
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        raise Exception("Unsupported Method!", request)


@require_http_methods(["GET", "DELETE"])
def api_sale(request, pk):
    '''GET an instance of Sale and
    DELETE an instance of Sale'''
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sale ID"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        raise Exception("Unsupported Method!", request)
