import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPeopleList from './sales_front/SalesPeopleList';
import SalesPersonForm from './sales_front/SalesPersonForm';
import CustomerList from './sales_front/CustomerList';
import CustomerForm from './sales_front/CustomerForm';
import SalesList from './sales_front/SalesList';
import SalesForm from './sales_front/SalesForm';
import TechnicianForm from './services_front/TechnicianForm';
import Technician_List from './services_front/Technician_List';
import Appointment_List from './services_front/Appointment_List';
import AppointmentForm from './services_front/AppointmentForm';
import Automobile_Form from './inventory_front/Automobile_Form';
import AutoList from './inventory_front/AutomobileList';
import Manufacturer_List from './inventory_front/Manufacturer_list';
import Service_List from './services_front/Service_History';
import Manufacturer_Form from './inventory_front/Manfacturer_Form';
import Model_Form from './inventory_front/Model_Form';
import ModelList from './inventory_front/Model_List';


function App() {

  const [salesPeople, setSalesPeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const[autos, setAutos] = useState([]);

  async function getSalesPeople() {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const {salespeople: salesPeople} = await response.json();
      setSalesPeople(salesPeople);
    } else {
      console.error("An Error Occurred While Fetching Salespeople Data");
    }
  }

  async function getCustomers() {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
      const {customers: customers} = await response.json();
      setCustomers(customers);
    } else {
      console.error("An Error Occurred While Fetching Customer Data");
    }
  }

  async function getSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const {sales: sales} = await response.json();
      setSales(sales);
    } else {
      console.error("An Error Occurred While Fetching Sales Data");
    }
  }

  async function getAutos() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const {autos: autos} = await response.json();
      setAutos(autos);
    }
  }

  useEffect(() => {
    getSalesPeople();
    getCustomers();
    getSales();
    getAutos();
  }, [])

return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/">
          <Route path="salespeople">
            <Route path="add" element={<SalesPersonForm getSalesPeople={getSalesPeople}/>}/>
            <Route index element={<SalesPeopleList getSalesPeople={getSalesPeople} salesPeople={salesPeople}/>}/>
          </Route>
          <Route path="customers">
            <Route path="add" element={<CustomerForm getCustomers={getCustomers}/>}/>
            <Route index element={<CustomerList getCustomers={getCustomers} customers={customers}/>}/>
          </Route>
          <Route path="sales">
            <Route path="add" element={<SalesForm getSales={getSales}/>}/>
            <Route index element={<SalesList getSales={getSales} sales={sales} setSales={setSales}/>}/>
          </Route>
            <Route path = "technician">
              <Route path="add" element={< TechnicianForm/>} />
              <Route path="list" element={< Technician_List />} />
            </Route>
            <Route path = "appointment">
              <Route path="list" element={< Appointment_List/>} />
              <Route path="reserve" element={< AppointmentForm />} />
            </Route>
            <Route path = "models">
              <Route path="add" element={< Automobile_Form />} />
              <Route path="list" element={< AutoList />} />
            </Route>
            <Route path = "manufacturer">
              <Route path="list" element={< Manufacturer_List/>} />
              <Route path="add" element={<Manufacturer_Form/>} />
            </Route>
            <Route path = "service">
              <Route path="list" element={<Service_List/>} />
            </Route>
            <Route path = "model">
              <Route path="add" element={<Model_Form/>} />
              <Route path="list" element={<ModelList/>} />
            </Route>
            <Route path = "automobiles">
              <Route path={autos.vin} element={<SalesForm getAutos={getAutos} />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
