import React, { useEffect, useState } from "react";

function Model_Form({getModels}) {
    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [add,setAdd] = useState(false)

    async function fetchManufacturers() {
        const manufacturer_url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(manufacturer_url)

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)

        }

    }

    useEffect(() => {
        fetchManufacturers();
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data  = {
            name:name,
            picture_url:picture_url,
            manufacturer_id: manufacturer,
        };


        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        };
        const response = await fetch(modelUrl, fetchConfig)
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
            setAdd(true);
        }
    }

    function handleChangeName(event) {
        const { value } = event.target;
        setName(value);
    }

    function handleChangeUrl(event) {
        const {value} = event.target;
        setPictureUrl(value);
    }

    function handleChangeManufacturer(event) {
        const {value} = event.target;
        setManufacturer(value);
    }

    let successMessage = 'alert alert-success d-none mb-0';
    let ModelForm= '';
    if (add) {
    successMessage = 'alert alert-success mb-0';
    ModelForm = 'd-none';
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Register a new Model</h1>
                <form className = {ModelForm} onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={picture_url} onChange={handleChangeUrl} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture url</label>
                    </div>
                        <div className="mb-3">
                        <select value={manufacturer} onChange={handleChangeManufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option>Choose a manufacturer</option>
                            {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className={successMessage} id="success-message">
              Your Model has been registered!
            </div>
                </div>
            </div>
        </div>
    );
}

export default Model_Form;
