import React,{useState, useEffect} from 'react';

function Manufacturer_List(){
    const [manufacturers, setManufacturers] = useState([]);

    async function load_Manfactuers() {
        const response = await fetch ('http://localhost:8100/api/manufacturers/')
        if (response.ok){
          const data = await response.json();
          setManufacturers(data.manufacturers);
        } else {
          console.log(response);
        }
    }

  useEffect(() => {
      load_Manfactuers();
  }, [])

  return (
        <div>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.id}>
                  <td>{ manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
      );
}

export default Manufacturer_List;
