import React, { useEffect, useState } from "react";

function Automobile_Form(){
    const [color,setColor]= useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [add, setAdd] = useState(false);
    const [models, setModels] = useState([])
    const [model, setModel] = useState('')

    async function fetchModels() {
        const model_url = 'http://localhost:8100/api/models/'
        const response = await fetch(model_url)
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchModels();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data  = {
            color: color,
            year:year,
            vin: vin,
            model_id: model,
        };
        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileUrl, fetchConfig)
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
            setAdd(true);
        }
    
    }
    function refreshPage(){
        window.location.reload();
    }

    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleChangeYear(event) {
        const {value} = event.target;
        setYear(value);
    }

    function handleChangeVIN(event) {
        const {value} = event.target;
        setVin(value);
    }

    function handleChangeModel(event) {
        const {value} = event.target;
        setModel(value);
    }

    let successMessage = 'alert alert-success d-none mb-0';
    let AutomobileForm= '';

    if (add) {
        successMessage = 'alert alert-success mb-0';
        AutomobileForm = 'd-none';
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add an Automobile</h1>
                <form className ={AutomobileForm} onSubmit={handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={year} onChange={handleChangeYear} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={vin} onChange={handleChangeVIN} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                        <div className="mb-3">
                    <select value={model} onChange={handleChangeModel}required name="model" id="model" className="form-select">
                            <option>Choose a model</option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                );
                            })}
                        </select>
                        </div>
                    <button onClick = {refreshPage} className="btn btn-success">Add</button>
                </form>
                <div className={successMessage} id="success-message">
              Your automobile has been added!
            </div>
                </div>
            </div>
        </div>
    );
}

export default Automobile_Form;
