import React, { useEffect, useState } from "react";

function Manufacturer_Form() {
    const[manufacturers, setManufacturers] = useState([]);
    const[name, setName] = useState('');

    async function fetchManufacturers() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchManufacturers();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name:name,
        }
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setName('');
        }
    }

    function handleChangeName(event) {
        const {value} = event.target;
        setName(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Manufacturer</h1>
              <form onSubmit={handleSubmit} id="add-manufacturer-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleChangeName} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default Manufacturer_Form;
