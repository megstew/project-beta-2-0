import React, {useState} from 'react';


function CustomerForm({getCustomers}) {
    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[phoneNumber, setPhoneNumber] = useState('');
    const[address, setAddress] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            phone_number: phoneNumber,
            address,
        }
        const customersUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customersUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setPhoneNumber('');
            setAddress('');
            getCustomers();
        }
    }

    function handleChangeFirstName(event) {
        const {value} = event.target;
        setFirstName(value);
    }

    function handleChangeLastName(event) {
        const {value} = event.target;
        setLastName(value);
    }

    function handleChangePhoneNumber(event) {
        const {value} = event.target;
        setPhoneNumber(value);
    }

    function handleChangeAddress(event) {
      const {value} = event.target;
      setAddress(value);
  }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
              <form onSubmit={handleSubmit} id="add-salesperson-form">
                <div className="form-floating mb-3">
                  <input value={firstName} onChange={handleChangeFirstName} placeholder="first_name" required type="text" name="firstName" id="firstName" className="form-control" />
                  <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={lastName} onChange={handleChangeLastName} placeholder="last_name" required type="text" name="lastName" id="lastName" className="form-control" />
                  <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={phoneNumber} onChange={handleChangePhoneNumber} placeholder="phone_number" required type="text" name="phoneNumber" id="phoneNumber" className="form-control" />
                  <label htmlFor="address">Phone Number</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={address} onChange={handleChangeAddress} placeholder="address" required type="text" name="address" id="address" className="form-control" />
                  <label htmlFor="address">Address</label>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default CustomerForm;
