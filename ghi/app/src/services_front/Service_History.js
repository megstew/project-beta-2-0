import React, { useState, useEffect } from 'react';

function Service_List() {
  const [appointments, setAppointments] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [filteredResults, setFilteredResults] = useState([]);

  async function loadAppointments() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      setFilteredResults(data.appointments);
    }
  }

  const searchItems = (searchValue) => {
    setSearchInput(searchValue);
    if (searchInput === '') {
      setFilteredResults(appointments);
    } else {
      const filteredData = appointments.filter((appointment) => {
        return appointment.vin.toLowerCase().includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    }
  };

  useEffect(() => {
    loadAppointments();
  }, []);

  return (
    <>
      <div>
        <h1>Service History</h1>
      </div>
      <div className="input-group mb-3">
        <input
          value={searchInput}
          onChange={(e) => setSearchInput(e.target.value)}
          type="text"
          className="form-control"
          placeholder="search vin"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <div className="input-group-append">
          <button
            onClick={() => searchItems(searchInput)}
            className="btn btn-outline-primary"
            type="button"
          >
            Search
          </button>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredResults.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.is_vip ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td>
                  {appointment.technician.first_name} {''} {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default Service_List;





























// import React,{useState, useEffect} from 'react';


// function Service_List(){
//     const [appointments, setAppointments] = useState([]);
//     const [searchInput, setSearchInput]= useState("")
//     const [filteredResults, setFilteredResults] = useState([]);
    

//     async function loadAppointments() {
//         const url = 'http://localhost:8080/api/appointments/';
//         const response = await fetch(url);
//         if (response.ok) {
//             const data = await response.json();
//             setAppointments(data.appointments);
//             setFilteredResults(data.appointments);
//         } 
//     }

    
    

//     const searchItems =(searchValue)=>{
//         setSearchInput(searchValue)
//         if(searchInput === ''){
//             // const filteredData = appointments.filter((item) => {
//                 // return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
//             // })
            

//             setFilteredResults(appointments)
//             // setFilteredResults(filteredData)
//         }else{
//             const filteredData = appointments.filter((item) => {
//                 return item.vin.toLowerCase().includes(searchInput.toLowerCase());
//                       });
//                       setFilteredResults(filteredData);
//                     }
//             // const filteredData = appointments.filter((vin = searchInput) => {
//             //     return Object.values(vin).join('').toLowerCase().includes(searchInput.toLowerCase())
//             // })
            
//             // setFilteredResults(filteredData)
//             // setFilteredResults(appointments)
//         }

//     }
//     useEffect(() => {
//         loadAppointments();
//         searchItems();
//         },[])
//     return (
//         <>
//         <div><h1>Service History</h1></div>
//         <div className = 'input-group mb-3'>
//             <input value={searchInput}  onChange ={(e)=>setSearchInput(e.target.value)} type ='text' class='form-control' placeholder = "search vin" aria-label = "Recipent's username" aria-describedby="basic-addon2"/>
//             <div class="input-group-append">
//             <button nClick={() => searchItems(searchInput)} class="btn btn-outline-primary" type="button">Search</button>
//             </div>
//         </div>
//         <table className="table table-striped">
//             <thead>
//             <tr>
//                 <th>VIN</th>
//                 <th>Is VIP?</th>
//                 <th>Customer</th>
//                 <th>Date</th>
//                 <th>Time</th>
//                 <th>Technician</th>
//                 <th>Reason</th>
//                 <th>Status</th>
//             </tr>
//             </thead>
//             <tbody>
//                 {/* {appointments.filter((filteredResults)=>{ */}
//                 {filteredResults.map(appointment=>{
//                 return (
//                     <tr key={appointment.id}>
//                         <td>{ appointment.vin }</td>
//                         <td> {appointment.is_vip ? "Yes" : "No" } </td>
//                         <td>{ appointment.customer }</td>
//                         <td> { new Date (appointment.date_time).toLocaleDateString() } </td>
//                         <td> { new Date (appointment.date_time).toLocaleTimeString() } </td>
//                         <td>{appointment.technician.first_name} {""} {appointment.technician.last_name} </td>
//                         <td>{ appointment.reason }</td>
//                         <td> {appointment.status} </td>
//                     </tr>
//                     );
//                 })}
                    
                    
                
//             {/* // })} */}
//             </tbody>
//         </table>
//         </>
//         );
//   }


//   export default Service_List;
