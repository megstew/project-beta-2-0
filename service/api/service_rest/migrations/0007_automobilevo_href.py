# Generated by Django 4.0.3 on 2023-07-27 02:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0006_alter_appointment_is_vip'),
    ]

    operations = [
        migrations.AddField(
            model_name='automobilevo',
            name='href',
            field=models.CharField(default='', max_length=200, unique=200),
            preserve_default=False,
        ),
    ]
