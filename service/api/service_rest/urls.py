from .views import *
from django.urls import path



urlpatterns = [
    path("technicians/", list_technician, name="list_technician"),
    path("technicians/<int:id>/",detail_technician,name ="detail_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>/",detail_appointments,name ="detail_appointments"),
    path("appointments/<int:id>/cancel",detail_appointments,name ="detail_appointments")

]
